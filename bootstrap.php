<?php
/**
 * @wordpress-plugin
 * Plugin Name: Dev CSV Product Exporter
 * Plugin URI: https://wpdesk.pl
 * Description: CSV Product Exporter for recruitment purpose
 * Version: 1.0
 * Requires: PHP 7.0
 * Author: WPDesk
 * Author URI: https://wpdesk.pl
 * Text Domain: dev-csv-product-exporter
 * Domain Path: /languages
 * WC requires at least: 3.6.3
 * WC tested up to: 3.6.3
 * Licence: ...
 * Licence URI: ...
 */

if ( ! defined( 'ABSPATH' ) ) {
	die();
}

define('WPDESK_DEV_CSV_EXPORTER_DIR', dirname(__FILE__));
define('WPDESK_DEV_CSV_EXPORTER_URL', plugins_url('', __FILE__));

$composer_autoloader = __DIR__ . '/vendor/autoload.php';
if ( is_readable( $composer_autoloader ) ) {
	/**@noinspection PhpIncludeInspection*/
	require( $composer_autoloader );
}

$plugin = \WPDesk\Init::create();

register_activation_hook(__FILE__, function() use ($plugin){
	$plugin->activate();
});

register_deactivation_hook(__FILE__, function() use ($plugin){
	$plugin->deactivate();
});

if (!$plugin->pluginRequirements()) {
	$plugin->register();
	$plugin->_links();
}





