<?php
	/**
	 * @var $productColumns array
	 * @var $variationColumns array
	 * @var $action string
	 */
?>
<div class="jumbotron">
    <div class="tool-box">
        <h3 class="title"><?php _e('Export Product CSV'); ?></h3>
        <p><?php _e('Export your products using this tool. This exported CSV will be in an importable format.'); ?></p>
        <p class="description"><?php _e('Click export to save your products to your computer.'); ?></p>
        <form action="<?= $action ?>" method="post">
            <table class="form-table">
                <tr>
                    <th>
                        <label for="v_limit"><?php _e('Limit'); ?></label>
                    </th>
                    <td>
                        <input type="number" min="0" step="1" name="limit" id="v_limit" placeholder="<?php _e('Unlimited'); ?>" class="input-text" />
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="v_offset"><?php _e('Offset'); ?></label>
                    </th>
                    <td>
                        <input type="number" min="0" step="1" name="offset" id="v_offset" placeholder="0" class="input-text" />
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="v_columns"><?php _e('Columns'); ?></label>
                    </th>
                    <td>
                        <ul>
                            <li><label><input type="checkbox" id="all-product-fields" name="all-product-fields" value="yes"><?php _e('Select/deselect all'); ?></label></li>
                        </ul>
                        <ul>
							<?php foreach ($productColumns as $key => $name): ?>
                                <li><label><input class="product_fields" type="checkbox" name="columns[]" value="<?php echo $key ?>"><?php echo $name; ?></label></li>
							<?php endforeach; ?>
                            <li><label><input class="product_fields" type="checkbox" name="columns[]" value="taxonomies"><?php _e('Taxonomies (cat/tags)')?></label></li>
                            <li><label><input class="product_fields" type="checkbox" name="columns[]" value="attributes"><?php _e('Attributes'); ?></label></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="wpdesk-product-export-headers"><?php _e('Output column headers'); ?></label></th>
                    <td>
                        <select name="wpdesk-product-export[headers]" id="wpdesk-product-export-headers">
                            <option value="label" selected="selected"><?php _e('Label'); ?></option>
                            <option value="key"><?php _e('Key'); ?></option>
                        </select>
                    </td>
                </tr>
            </table>
            <input name="wpdesk-product-export-action" type="hidden" value="product" />
            <p class="submit"><input type="submit" class="button" value="<?php _e('Export Products'); ?>" /></p>
        </form>
    </div>

    <div class="tool-box">
        <h3 class="title"><?php _e('Export Product Variations CSV'); ?></h3>
        <p><?php _e('Export your product variations using this tool. This exported CSV will be in an importable format.'); ?></p>
        <p class="description"><?php _e('Click export to save your products variations to your computer.'); ?></p>
        <form action="<?= $action ?>" method="post">
            <table class="form-table">
                <tr>
                    <th>
                        <label for="limit"><?php _e('Limit'); ?></label>
                    </th>
                    <td>
                        <input type="number" min="0" step="1" name="limit" id="limit" placeholder="<?php _e('Unlimited'); ?>" class="input-text" />
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="offset"><?php _e('Offset'); ?></label>
                    </th>
                    <td>
                        <input type="number" min="0" step="1" name="offset" id="offset" placeholder="0" class="input-text" />
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="columns"><?php _e('Columns'); ?></label>
                    </th>
                    <td>
                        <ul>
                            <li><label><input type="checkbox" id="all-variation-fields" name="all-variation-fields" value="yes"><?php _e('Select/deselect all'); ?></label></li>
                        </ul>
                        <ul>
							<?php foreach ($variationColumns as $key => $name): ?>
                                <li><label><input class="variation_fields" type="checkbox" name="columns[]" value="<?php echo $key ?>"><?php echo $name; ?></label></li>
							<?php endforeach; ?>
                            <li><label><input class="variation_fields" type="checkbox" name="columns[]" value="taxonomies"><?php _e('Taxonomies (cat/tags)'); ?></label></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="wpdesk-product-export-headers"><?php _e('Output column headers'); ?></label></th>
                    <td>
                        <select name="wpdesk-product-export[headers]" id="wpdesk-product-export-headers">
                            <option value="label" selected="selected"><?php _e('Label'); ?></option>
                            <option value="key"><?php _e('Key'); ?></option>
                        </select>
                    </td>
                </tr>
            </table>
            <input name="wpdesk-product-export-action" type="hidden" value="variation" />
            <p class="submit"><input type="submit" class="button" value="<?php _e('Export Variations'); ?>" /></p>
        </form>
    </div>
</div>