(function($){
    $(document).ready(function(){
        $('#all-product-fields').change(function(){
            if($(this).is(":checked")){
                $('.product_fields').attr('checked', 'checked');
            } else {
                $('.product_fields').removeAttr('checked');
            }
        });
        $('#all-variation-fields').change(function(){
            if($(this).is(":checked")){
                $('.variation_fields').attr('checked', 'checked');
            } else {
                $('.variation_fields').removeAttr('checked');
            }
        });
    });
})(jQuery);