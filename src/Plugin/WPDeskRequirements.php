<?php

namespace WPDesk\Plugin;


use DateTime;

trait WPDeskRequirements
{
	
	private $plugin_version = '1.0';
	private $plugin_release_timestamp = '2019-05-17 18:29';
	private $plugin_name = 'Dev CSV Product Exporter';
	private $plugin_class_name = 'DevCsvExporter';
	private $plugin_text_domain = 'dev-csv-product-exporter';
	
	public function requirements()
	{
		if (!class_exists('WPDesk_Basic_Requirement_Checker')) {
			require_once(__DIR__ . '/../../vendor/wpdesk/wp-basic-requirements/src/Basic_Requirement_Checker.php');
		}
		
		define('DEV_CSV_EXPORTER', $this->plugin_version);
		define($this->plugin_class_name, $this->plugin_version);
		
		$requirements_checker = new \WPDesk_Basic_Requirement_Checker(
			__FILE__,
			$this->plugin_name,
			$this->plugin_text_domain,
			'7.0',
			'5.2'
		);
		$requirements_checker->add_plugin_require('woocommerce/woocommerce.php', 'Woocommerce');
		
		if ($requirements_checker->are_requirements_met()) {
			if (!class_exists('WPDesk_Plugin_Info')) {
				require_once __DIR__ . '/../../vendor/wpdesk/wp-basic-requirements/src/Plugin/Plugin_Info.php';
			}
			
			$plugin_info = new \WPDesk_Plugin_Info();
			$plugin_info->set_plugin_file_name(plugin_basename(__FILE__));
			$plugin_info->set_plugin_dir(dirname(__FILE__));
			$plugin_info->set_class_name($this->plugin_class_name);
			$plugin_info->set_version($this->plugin_version);
			$plugin_info->set_product_id($this->plugin_text_domain);
			$plugin_info->set_text_domain($this->plugin_text_domain);
			$plugin_info->set_release_date(new DateTime($this->plugin_release_timestamp));
			$plugin_info->set_plugin_url(plugins_url(dirname(plugin_basename(__FILE__))));
		} else {
			$requirements_checker->render_notices();
			$requirements_checker->disable_plugin();
		}
	}
}