<?php
/**
 * @Dev @vasilguruli.
 * PHP is Awesome. Unfortunately life isn't sometimes,
 * but if you don't fall down then you'll never learn how to get up.
 */

namespace WPDesk\Plugin;

use WPDesk\PluginInterface\Plugin;

abstract class DevCsvExporterInit extends Service implements Plugin
{
	use WPDeskRequirements;
	
	public const WPDESK_DEV_CSV_EXPORTER_REGISTRATION_ACTION = 'plugins_loaded';
	
	public function pluginRequirements()
	{
		$this->requirements();
	}
	
	public function activate()
	{
		// Do something when activate
	}
	
	public function deactivate()
	{
		// Do something when deactivate
	}
	
	
}