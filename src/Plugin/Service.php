<?php

namespace WPDesk\Plugin;


use Exception;

abstract class Service
{
	/** @var array $services */
	public $services = [];
	
	/**
	 * @param $key
	 * @param $name
	 * @param array $params
	 * @throws Exception
	 */
	public function setDetails($key, $name, array $params)
	{
		if ($this->exists($key)) {
			throw new Exception(sprintf('Details info for %s are already set.', $key));
		}
		
		$serviceDetails = [
			'name' => $name,
			'params' => $params
		];
		
		$this->services[$key] = $serviceDetails;
	}
	
	
	/**
	 * @param string $key
	 *
	 * @return string
	 */
	public function getClassName($key)
	{
		return '\\' . $this->services[$key]['name'];
	}
	
	public function exists($key)
	{
		return isset($this->services[$key]);
	}
	
	public function addService()
	{
		foreach ($this->services as $service) {
			if (class_exists($service['name'])) {
				new $service['name']();
			} else {
				throw new Exception(sprintf("Class %s could not be found", $service['name']));
			}
		}
	}
}