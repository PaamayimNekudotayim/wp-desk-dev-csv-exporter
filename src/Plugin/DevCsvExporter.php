<?php

namespace WPDesk\Plugin;

class DevCsvExporter extends DevCsvExporterInit
{
	
	public function register()
	{
		add_action(DevCsvExporterInit::WPDESK_DEV_CSV_EXPORTER_REGISTRATION_ACTION,
			[$this, 'wpdesc_dev_csv_exporter_register_plugin']);
	}
	
	/**
	 * @throws \Exception
	 */
	public function wpdesc_dev_csv_exporter_register_plugin()
	{
		$this->setDetails('wpdesk.csv.exporter', '\\WPDesk\\Exporter\\ExporterMenu', []);
		$this->addService();
	}
	
	public function _links()
	{
		add_filter( 'plugin_action_links_wp-desk-dev-csv-exporter/bootstrap.php',
			function ($links) {
				$links[] = '<a href="' . esc_url(admin_url('admin.php?page=dev-csv-exporter')) . '">Settings</a>';
				$links[] = '<a href="#" target="_blank">Documentation</a>';
				$links[] = '<a href="#" target="_blank">Support</a>';
				
				return $links;
			});
		
	}
}
