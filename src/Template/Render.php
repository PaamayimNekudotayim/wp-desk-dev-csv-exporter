<?php

namespace WPDesk\Template;

use Exception;

class Render
{
	/** @var array */
	protected static $locations = [];
	
	/**
	 * @param $key
	 * @param $path
	 * @throws Exception
	 */
	public static function addLocation($key, $path)
	{
		if (!isset(self::$locations[$key])) {
			self::$locations[$key] = $path;
		} else {
			throw new Exception(sprintf(__('The key [%s] already exists.'), $key));
		}
	}
	
	public static function get($key, $template, array $environment)
	{
		ob_start();
		self::output($key, $template, $environment);
		
		return ob_get_clean();
	}
	
	/**
	 * @param $key
	 * @param $template
	 * @param array $environment
	 * @throws Exception
	 */
	public static function output($key, $template, array $environment)
	{
		$file = self::locateTemplate($key, $template);
		extract($environment);
		/** @noinspection PhpIncludeInspection */
		require($file);
	}
	
	/**
	 * @param $key
	 * @param $template
	 * @return string
	 * @throws Exception
	 */
	public static function locateTemplate($key, $template)
	{
		$file = locate_template([strtolower($key) . '/' . $template . '.php'], false, false);
		if (empty($file)) {
			if (!isset(self::$locations[$key])) {
				throw new Exception(sprintf(__('The key [%s] does not exist.'), $key));
			}
			
			$file = self::$locations[$key] . '/templates/' . $template . '.php';
		}
		
		return $file;
	}
}