<?php

namespace WPDesk\PluginInterface;


interface Register
{
	public function register();
}