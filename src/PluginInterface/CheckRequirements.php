<?php

namespace WPDesk\PluginInterface;


interface CheckRequirements
{
	public function pluginRequirements();
}