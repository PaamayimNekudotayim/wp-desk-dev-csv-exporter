<?php

namespace WPDesk\PluginInterface;


interface Plugin extends Activate, Deactivate, Register, CheckRequirements
{
	
}