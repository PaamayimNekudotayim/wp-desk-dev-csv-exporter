<?php

namespace WPDesk\PluginInterface;


interface Deactivate
{
	public function deactivate();
}