<?php

namespace WPDesk;

use WPDesk\Plugin\DevCsvExporter;
use WPDesk\PluginInterface\Plugin;

class Init
{
	/**
	 * @return Plugin
	 */
	public static function create(): Plugin
	{
		static $plugin = null;
		
		if (null === $plugin) {
			$plugin = new DevCsvExporter();
		}
		
		return $plugin;
	}
}