<?php

namespace WPDesk\Exporter;


class Helper
{
	/**@var $postType*/
	protected $postType;
	
	/**
	 * @param $value
	 * @return string
	 */
	protected function format_value_csv($value)
	{
		if (is_string($value)) {
			$formattedValue = '" ' . str_replace('"', '""', $value) . '",';
		} elseif (is_array($value)) {
			$formattedValue = '"';
			foreach ($value as $item) {
				$formattedValue .= ' ' . str_replace('"', '""', $item) . ',';
			}
			$formattedValue .= '",';
		} else {
			$formattedValue = str_replace('"', '""', $value) . ',';
		}
		
		return $this->format_data($formattedValue);
	}
	
	/**
	 * @param $metaValue
	 * @param $meta
	 * @return false|string
	 */
	protected function format_export_meta($metaValue, $meta)
	{
		if ($meta == '_sale_price_dates_from' || $meta == '_sale_price_dates_to') {
			if ($metaValue) {
				return date('Y-m-d', $metaValue);
			}
		}
		
		return $metaValue;
	}
	
	protected function format_data($data)
	{
		$data = (string)$data;
		$enc = mb_detect_encoding($data, 'UTF-8, ISO-8859-1', true);
		if ($enc != 'UTF-8') {
			$data = utf8_encode($data);
		}
		
		return $data;
	}
	
	protected function get_all_meta_keys()
	{
		global $wpdb;
		
		$meta = $wpdb->get_col($wpdb->prepare(
			"SELECT DISTINCT pm.meta_key
                FROM {$wpdb->postmeta} AS pm
                LEFT JOIN {$wpdb->posts} AS p ON p.ID = pm.post_id
                WHERE p.post_type = %s
                AND p.post_status IN ( 'publish', 'pending', 'private', 'draft' )",
			$this->postType
		));
		sort($meta);
		
		return $meta;
	}
	
	protected function get_all_product_attributes()
	{
		global $wpdb;
		
		$results = $wpdb->get_col($wpdb->prepare(
			"SELECT DISTINCT pm.meta_value
                FROM {$wpdb->postmeta} AS pm
                LEFT JOIN {$wpdb->posts} AS p ON p.ID = pm.post_id
                WHERE p.post_type = %s
                AND p.post_status IN ( 'publish', 'pending', 'private', 'draft' )
                AND pm.meta_key = 'product_attributes'",
			$this->postType
		));
		
		$result = [];
		
		if (!empty($results)) {
			foreach ($results as $productAttributes) {
				$attributes = maybe_unserialize(maybe_unserialize($productAttributes));
				if (!empty($attributes) && is_array($attributes)) {
					foreach ($attributes as $key => $attribute) {
						if (isset($attribute['position']) && isset($attribute['visible']) && isset($attribute['variation'])) {
							$key = 'pa_' . $key;
							$result[$key] = $key;
						}
					}
				}
			}
		}
		sort($result);
		
		return $result;
	}
}