<?php

namespace WPDesk\Exporter;

use WPDesk\Template\Render;

class ExporterMenu
{
	const PAGE = 'woocommerce_page_dev-csv-exporter';
	
	/**@var $productColumns array */
	private $productColumns;
	
	/**@var $variationColumns array */
	private $variationColumns;
	
	/**@var $itemFields array */
	private $itemFields;
	
	/**
	 * Exporter constructor.
	 * @throws \Exception
	 */
	public function __construct()
	{
		Render::addLocation('dev.csv.exporter', WPDESK_DEV_CSV_EXPORTER_DIR);
		add_action('admin_menu', [$this, 'add_menu_page']);
		add_action('admin_enqueue_scripts', [$this, 'admin_scripts']);
		add_action('init', [$this, 'export_products'], 10);
	}
	
	
	public function export_products()
	{
		if (isset($_POST['wpdesk-product-export-action'])) {
			switch ($_POST['wpdesk-product-export-action']) {
				case 'product':
					$this->product_exporter('product', $_POST['wpdesk-product-export']['headers']);
					break;
				case 'variation':
					$this->product_exporter('product_variation', $_POST['wpdesk-product-export']['headers']);
					break;
			}
		}
	}
	
	private function product_exporter($postType, $headersType)
	{
		if ($postType == 'product') {
			$csvColumns = $this->get_product_columns();
		} else {
			$csvColumns = $this->get_variation_columns();
		}
		new ExporterProducts($csvColumns, $headersType, $postType);
	}
	
	public function add_menu_page()
	{
		add_submenu_page('woocommerce', 'DEV CSV Exporter',
			'DEV CSV Exporter', 'manage_options', 'dev-csv-exporter', [$this, 'exporter_submenu']);
	}
	
	/**
	 * @throws \Exception
	 */
	public function exporter_submenu()
	{
		if (!current_user_can('manage_options')) {
			wp_die(__('You do not have sufficient permissions to access this page.'));
		}
		
		Render::output('dev.csv.exporter', 'admin/exporter', [
			'productColumns' => $this->get_product_columns(),
			'variationColumns' => $this->get_variation_columns(),
			'action' => esc_url(admin_url('admin.php?page=dev-csv-exporter&status=export_csv'))
		]);
	}
	
	public function admin_scripts($page)
	{
		if ($page !== self::PAGE) {
			return;
		}
		
		wp_enqueue_script('wpdesk.jquery', 'https://code.jquery.com/jquery-3.3.1.slim.min.js',
			[], '3.3.1', true
		);
		wp_enqueue_script(
			'wpdesk.cloudflare', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js',
			[], '1.14.7', true
		);
		wp_enqueue_script(
			'wpdesk.bootstrapcdn', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
			[], '4.3.1', true
		);
		wp_enqueue_script('wpdesk.admin.script', WPDESK_DEV_CSV_EXPORTER_URL . '/assets/js/admin_script.js'
			, ['wpdesk.jquery'], '1.0', true);
		wp_enqueue_style('wpdesk.admin.bootstrap',
			'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
			'', '4.3.1');
		wp_enqueue_style('wpdesk.admin.style', WPDESK_DEV_CSV_EXPORTER_URL . '/assets/css/admin_style.css',
			'wpdesk.admin.bootstrap');
	}
	
	private function get_product_columns()
	{
		if (!is_array($this->productColumns)) {
			$this->productColumns = [
				'post_title' => 'Post title',
				'ID' => 'Product ID',
				'_sku' => 'SKU',
				'_stock' => 'Stock',
				'_regular_price' => 'Regular price',
				'_sale_price' => 'Sale price',
				'_weight' => 'Weight',
				'_length' => 'Length',
				'_width' => 'Width',
				'_height' => 'Height',
			];
		}
		
		return $this->productColumns;
	}
	
	private function get_variation_columns()
	{
		if (!is_array($this->variationColumns)) {
			$this->variationColumns = [
				'post_parent' => 'Post parent ID',
				'ID' => 'Variation ID',
				'post_status' => 'Post status',
				'_sku' => 'SKU',
				'_stock' => 'Stock',
				'_regular_price' => 'Regular price',
				'_sale_price' => 'Sale price',
				'_weight' => 'Weight',
				'_length' => 'Length',
				'_width' => 'Width',
				'_height' => 'Height',
			];
		}
		
		return $this->variationColumns;
	}
}