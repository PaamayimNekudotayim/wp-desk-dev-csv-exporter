<?php

namespace WPDesk\Exporter;

use League\Csv\Reader;
use League\Csv\Writer;
use SplTempFileObject;

class ExporterProducts extends Helper
{
	/**@var $csvColumns*/
	protected $csvColumns;
	
	/**@var $headersType*/
	protected $headersType;
	
	public function __construct($csvColumns, $headersType, $postType = 'product')
	{
		$this->csvColumns = $csvColumns;
		$this->postType = $postType;
		$this->headersType = $headersType;
		$this->export();
	}
	
	
	private function export()
	{
		global $wpdb;
		
		if ($this->postType == 'product') {
			$productTaxonomies = get_object_taxonomies($this->postType, 'name');
		} else {
			$productTax = get_object_taxonomies('product', 'name');
			$productTaxonomies['product_type'] = $productTax['product_type'];
		}
		
		if (!empty($_POST['columns'])) {
			$exportColumns = $_POST['columns'];
		} else {
			$exportColumns = [];
		}
		
		if (!empty($_POST['limit'])) {
			$numberPosts = intval($_POST['limit']);
		} else {
			$numberPosts = -1;
		}
		
		if (!empty($_POST['offset'])) {
			$offset = intval($_POST['offset']);
		} else {
			$offset = 0;
		}
		
		$productArgs = [
			'numberposts' => $numberPosts,
			'post_status' => ['publish', 'pending', 'private', 'draft'],
			'post_type' => $this->postType,
			'orderby' => 'ID',
			'order' => 'ASC',
			'offset' => $offset,
		];
		
		if ($this->postType == 'product_variation') {
			$productArgs['orderby'] = 'post_parent menu_order';
		}
		
		$products = get_posts($productArgs);
		
		if (!$products || is_wp_error($products)) {
			// Add error  messages
			//	$this->errors[] = 'Some Error';
		}
		
		$allMetaKeys = $this->get_all_meta_keys();
		$foundAttributes = $this->get_all_product_attributes();
		
		$wpdb->hide_errors();
		@set_time_limit(0);
		
		$foundProductMeta = [];
		
		foreach ($allMetaKeys as $meta) {
			if ($meta && in_array($meta, array_keys($this->csvColumns)) && !empty($meta)) {
				$foundProductMeta[] = $meta;
			}
		}
		
		foreach ($products as $product) {
			$metaData = get_post_custom($product->ID);
			$product->meta = new \stdClass;
			$product->attributes = new \stdClass;
		
			foreach ($metaData as $meta => $value) {
				//jackBug($value);
				if ($meta && in_array($meta, array_keys($this->csvColumns)) && !empty($meta)) {
					$metaValue = maybe_unserialize($value[0]);
					
					if (is_array($metaValue)) {
						if ($meta == 'variation_data' && !empty($metaValue)) {
							$metaString = '';
							foreach ($metaValue as $key => $_value) {
								$key = str_replace('tax_', '', $key);
								$metaString .= sprintf('%s,%s|', $key, $_value);
							}
							$metaValue = $metaString;
						}elseif (isset($metaValue[0]) && !is_array($metaValue[0])) {
							$metaValue = implode('|', $metaValue);
						} else {
							$metaValue = '';
						}
					}
					$product->meta->$meta = $this->format_export_meta($metaValue, $meta);
				}
			}
			
			if (isset($metaData['product_attributes'][0])) {
				$attributes = maybe_unserialize($metaData['product_attributes'][0]);
				if ($attributes) {
					foreach ($attributes as $key => $attribute) {
						if ($key && isset($attribute['position']) && isset($attribute['visible']) &&
							isset($attribute['variation'])) {
							$key = 'pa_' . $key;
							$attribute['name'] = 'pa_' . $attribute['name'];
							
							if ($attribute['is_taxonomy'] == 1) {
								$terms = wp_get_post_terms($product->ID, $key, ['fields' => 'names']);
								if (!is_wp_error($terms)) {
									$attributeValue = implode('|', $terms);
								} else {
									$attributeValue = '';
								}
							} else {
								$key = $attribute['name'];
								$attributeValue = $attribute['value'];
							}
							
							$attributeData = $attribute['position'] . '|' . $attribute['visible'] . '|' .
																				$attribute['variation'];
							
							if (isset($metaData['default_attributes'][0])) {
								$defaultAttributes = maybe_unserialize($metaData['default_attributes'][0]);
							} else {
								$defaultAttributes = '';
							}
							
							if (is_array($defaultAttributes) && isset($defaultAttributes[$key])) {
								$defaultAttribute = $defaultAttributes[$key];
							} else {
								$defaultAttribute = '';
							}
							
							$product->attributes->$key = [
								'value' => $attributeValue,
								'data' => $attributeData,
								'default' => $defaultAttribute
							];
						}
					}
				}
			}
		}
		
		$foundProductMeta = array_diff($foundProductMeta, array_keys($this->csvColumns));
		
		if ($this->headersType == 'label') {
			$file = "\xEF\xBB\xBF";
		} else {
			$file = '';
		}
		
		if ($this->postType == 'product_variation') {
			$file .= '" Parent",';
			if ($this->headersType == 'label') {
				$file .= '" Parent SKU",';
			} else {
				$file .= '" parent_sku",';
			}
		}
		
		$exportColumns = array_flip($exportColumns);

		foreach ($this->csvColumns as $column => $value) {
			if (empty($exportColumns) || array_key_exists($column, $exportColumns)) {
				if ($this->headersType == 'label') {
					$file .= $this->format_value_csv(esc_attr($value));
				} else {
					$file .= $this->format_value_csv(esc_attr($column));
				}
			}
		}
		
		if (empty($exportColumns) || array_key_exists('images', $exportColumns)) {
			if ($this->headersType == 'label') {
				$file .= '" Images",';
			} else {
				$file .= '" images",';
			}
		}
		
		if (empty($exportColumns) || array_key_exists('taxonomies', $exportColumns)) {
			foreach ($productTaxonomies as $taxonomy) {
				//jackBug($taxonomy);
				if (!strstr($taxonomy->name, 'pa_')) {
					if ($this->headersType == 'label') {
						if ($taxonomy->name == 'product_cat') {
							$file .= '" Product Categories",';
						} elseif ($taxonomy->name == 'product_tag') {
							$file .= '" Product Tags",';
						} else {
							$file .= '"  ' . ucwords(str_replace('_', ' ', $taxonomy->name)) . '",';
						}
					} else {
						// $file .= '" tax: '.$taxonomy->name.'",';
					}
				}
			}
		}
		
		if (empty($exportColumns) || array_key_exists('meta', $exportColumns)) {
			foreach ($foundProductMeta as $productMeta) {
				if ($this->headersType == 'label') {
					$file .= '" Meta: ' . $productMeta . '",';
				} else {
					$file .= '" meta: ' . $productMeta . '",';
				}
			}
		}
		
		if (empty($exportColumns) || array_key_exists('attributes', $exportColumns)) {
			foreach ($foundAttributes as $attribute) {
				if ($this->headersType == 'label') {
					$file .= '" Attribute: ' . $attribute . '",';
					$file .= '" Attribute data: ' . $attribute . '",';
					$file .= '" Attribute default: ' . $attribute . '",';
				} else {
					$file .= '" attribute: ' . $attribute . '",';
					$file .= '" attribute_data: ' . $attribute . '",';
					$file .= '" attribute_default: ' . $attribute . '",';
				}
			}
		}
		
		$file .= "\r\n";

		foreach ($products as $product) {
			if ($this->postType == 'product_variation') {
				$postParentTitle = get_the_title($product->post_parent);
				if (!$postParentTitle) {
					continue;
				} else {
					$file .= $this->format_value_csv($postParentTitle);
					$parentSku = get_post_meta($product->ID, 'sku', true);
					$file .= $this->format_value_csv($parentSku);
				}
			}
			
			foreach ($this->csvColumns as $column => $value) {
				if (empty($exportColumns) || array_key_exists($column, $exportColumns)) {
					if (isset($product->meta->$column)) {
						$file .= $this->format_value_csv($product->meta->$column);
					} elseif (isset($product->$column) && !is_array($product->$column)) {
						$file .= $this->format_value_csv($product->$column);
					} else {
						$file .= ',';
					}
				}
			}
			
			if (empty($exportColumns) || array_key_exists('taxonomies', $exportColumns)) {
				foreach ($productTaxonomies as $taxonomy) {
					if (!strstr($taxonomy->name, 'pa_')) {
						$terms = wp_get_post_terms($product->ID, $taxonomy->name, ["fields" => "names"]);
						$file .= $this->format_value_csv(implode('|', $terms));
					}
				}
			}
			
			if (empty($exportColumns) || array_key_exists('meta', $exportColumns)) {
				foreach ($foundProductMeta as $productMeta) {
					if (isset($product->meta->$productMeta)) {
						$file .= $this->format_value_csv($product->meta->$productMeta);
					} else {
						$file .= ',';
					}
				}
			}
			
			if (empty($exportColumns) || array_key_exists('attributes', $exportColumns)) {
				foreach ($foundAttributes as $attribute) {
					if (isset($product->attributes) && isset($product->attributes->$attribute)) {
						$values = $product->attributes->$attribute;
						$file .= $this->format_value_csv($values['value']);
						$file .= $this->format_value_csv($values['data']);
						$file .= $this->format_value_csv($values['default']);
					} else {
						$file .= ',';
						$file .= ',';
						$file .= ',';
					}
				}
			}
			
			$file .= "\r\n";
		}
		
		if ($this->postType == 'product') {
			$filename = 'products_' . date('d.m.Y');
		} else {
			$filename = 'product_variations_' . date('d.m.Y');
		}
		
		ob_end_clean();
		
		header('Content-Type: text/csv; charset=UTF-8');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Content-Type: text/csv');
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv";');
		header('Content-Length: ' . strlen($file));
		
		if (file_put_contents(WPDESK_DEV_CSV_EXPORTER_DIR . '/temp/' . $filename . '.csv', $file)) {
			$reader = Reader::createFromPath(WPDESK_DEV_CSV_EXPORTER_DIR . '/temp/' . $filename . '.csv', 'r');
			$reader->output();
		} else {
			echo $file;
		}
		
		die;
	}
}